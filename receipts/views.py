from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from receipts.models import Account, Receipt, ExpenseCategory
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm

# Create your views here.

@login_required
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {'receipts': receipts}
    return render(request, 'receipts/receipt_list.html', context)

# def query_queryset(self):
#     return Receipt.objects.filter(purchaser=self.request.user)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
      # To redirect to the detail view of the model, use this:
            form = form.save(False)
            form.purchaser = request.user
            form.save()
            return redirect('home')

      # To add something to the model, like setting a user,
      # use something like this:
      #
      # model_instance = form.save(commit=False)
      # model_instance.user = request.user
      # model_instance.save()
      # return redirect("detail_url", id=model_instance.id)
    else:
        form = ReceiptForm()

    context = {
        "form": form
    }

    return render(request, "receipts/create_view.html", context)

@login_required
def expense_category_list(request):
    expense_categories = ExpenseCategory.objects.filter(
        owner=request.user)
    context = {
        'expense_categories': expense_categories,
    }
    return render(request, 'receipts/expensecategory_list.html', context)


@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        'accounts': accounts,
    }
    return render(request, 'receipts/account_list.html', context)

@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
      # To redirect to the detail view of the model, use this:
            form = form.save(False)
            form.owner = request.user
            form.save()
            return redirect('category_list')

      # To add something to the model, like setting a user,
      # use something like this:
      #
      # model_instance = form.save(commit=False)
      # model_instance.user = request.user
      # model_instance.save()
      # return redirect("detail_url", id=model_instance.id)
    else:
        form = ExpenseCategoryForm()

    context = {
        "form": form
    }

    return render(request, "receipts/create_category.html", context)

@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
      # To redirect to the detail view of the model, use this:
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect('account_list')


      # To add something to the model, like setting a user,
      # use something like this:
      #
      # model_instance = form.save(commit=False)
      # model_instance.user = request.user
      # model_instance.save()
      # return redirect("detail_url", id=model_instance.id)
    else:
        form = AccountForm()

    context = {
        "form": form
    }

    return render(request, "receipts/create_account.html", context)
