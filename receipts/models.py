
from django.db import models
from django.contrib.auth.models import User
# Create your models here.
class ExpenseCategory(models.Model): # is the value that we can apply to the receipts like gas or entertainment
    name = models.CharField(max_length=50)
    owner= models.ForeignKey(User,related_name = "categories", on_delete = models.CASCADE)


    def __str__(self):
        return self.name




class Account(models.Model): # is the way that we paid for it such as with a specific creidt card or bank account
    name = models.CharField(max_length=100)
    number = models.CharField(max_length=20)
    owner = models.ForeignKey(User, related_name ="accounts", on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Receipt(models.Model): # is the primary thing that this application keeps track of for accounting purposes
    vendor = models.CharField(max_length=200)
    total = models.DecimalField(max_digits =10, decimal_places = 3)
    tax = models.DecimalField(max_digits=10, decimal_places=3)
    date = models.DateTimeField()
    purchaser = models.ForeignKey(User, related_name="receipts", on_delete=models.CASCADE)
    category = models.ForeignKey(ExpenseCategory, related_name="receipts",on_delete=models.CASCADE)
    account = models.ForeignKey(Account, related_name = "receipts", on_delete=models.CASCADE,null=True)


    def __str__(self):
        return self.vendor
